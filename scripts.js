// 1)
let sortDirection = false;
let personDataJs = [
  {idjs:2,namejs:'Zoran',surnamejs:'Lipac',agejs:23, genderjs:'male'},
  {idjs:1,namejs:'Milan',surnamejs:'Kobac',agejs:43, genderjs:'male'},
  {idjs:3,namejs:'Sanja',surnamejs:'Kotur',agejs:53, genderjs:'female'} ];

// 3)
window.onload=()=>{
  loadTableData(personDataJs);
};

// 2)
function loadTableData(personDataJs){
  const tableBodyJs = document.getElementById('tabledata');
  let dataHtml = '';

  for(let personjs of personDataJs){
    dataHtml += `<tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                  <td class="py-4 px-6">${personjs.idjs}</td>
                  <td class="py-4 px-6">${personjs.namejs}</td>
                  <td class="py-4 px-6">${personjs.surnamejs}</td>
                  <td class="py-4 px-6">${personjs.agejs}</td>
                  <td class="py-4 px-6">${personjs.genderjs}</td>
                </tr>`; 
  }

  tableBodyJs.innerHTML = dataHtml;
}

// 4)
function sortColumn(columnName) {
  const dataType = typeof personDataJs[0][columnName];
  sortDirection = !sortDirection;

  //6)
  switch(dataType){
    case 'number':
    sortNumberColumn(sortDirection,columnName);
    break;
  }

  loadTableData(personDataJs);
}

// 5)
function sortNumberColumn(sort, columnName){
  personDataJs = personDataJs.sort((p1,p2)=>{
    return sort ? p1[columnName] - p2[columnName] : p2[columnName] - p1[columnName] 
  });
}



// 7)
function myFunction() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}